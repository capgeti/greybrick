import bge

def fixAllFont():
    for scene in bge.logic.getSceneList():
        for object in scene.objects:
            if object.__class__ == bge.types.KX_FontObject:
                object.resolution = 2
