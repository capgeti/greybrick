import random
import bge

__author__ = 'capgeti'

def move(cont):
    game = bge.logic.getCurrentScene().objects['ControllerEmpty']
    if not game['gameOver']:
        cont.owner.applyMovement((0, -game['speed'], 0))

def loop(cont):
    game = cont.owner
    if game['gameOver']:
        return

    sce = bge.logic.getCurrentScene()
    if game['last'] > (4 / game['speed']):
        game['last'] = 0
        p = sce.objects['p' + str(random.randint(1, 4))]
        brick = sce.addObject('Brick', p)
        brick['id'] = game['count']
        game['count'] += 1

        if game['speed'] > 0.15 and game['baseSpeed'] > 0.0004:
            game['baseSpeed'] *= 0.96
        game['speed'] += game['baseSpeed']
    game['last'] += 1

def gameOver(game, obj):
    game['gameOver'] = True
    bge.logic.addScene("EndGame", 1)
    bge.logic.getCurrentScene().suspend()
    bge.logic.sendMessage("points", str(game['klick']))
    obj.color = (1.0, 0, 0, 1)

def click(cont):
    game = bge.logic.getCurrentScene().objects['ControllerEmpty']
    if game['gameOver']:
        return

    sen = cont.sensors
    if sen['click'].positive and sen['over'].positive:
        if (game['klick'] == cont.owner['id']):
            cont.owner.endObject()
            game['klick'] += 1
            bge.logic.getCurrentScene().objects['points'].text = str(game['klick'])
        else:
            gameOver(game, cont.owner)

def collision(cont):
    game = cont.owner
    sens = cont.sensors['Collision']
    if sens.positive:
        gameOver(game, sens.hitObject)