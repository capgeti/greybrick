import bge

__author__ = 'capgeti'


def init(cont):
    bge.logic.loadGlobalDict()
    if "name" in bge.logic.globalDict:
        cont.owner.text = bge.logic.globalDict['name']

def inputName(cont):
    obj = cont.owner
    if cont.sensors['key'].positive:
        bge.logic.globalDict['name'] = obj.text
        bge.logic.saveGlobalDict()
        if len(obj.text) > 10:
            obj.text = obj.text[:10]
