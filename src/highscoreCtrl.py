import urllib.request as urllib


__author__ = 'capgeti'


def init(cont):
    textObj = cont.owner

    resp = urllib.urlopen("http://greybrick.kilu.de/highscore.php")

    result = eval(resp.read().decode("UTF-8"))

    textObj.text = ''
    for score in result['highscore']:
        textObj.text += str(score['rank']) + ". " + str(score['points']) + " von " + score['name'] + " am " + score[
            'date'] + "\n"
