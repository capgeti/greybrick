import urllib.request
from urllib.parse import urlencode
import bge


__author__ = 'capgeti'


def initEndGameScene(cont):
    msg = cont.sensors['Message']
    if msg.positive:
        bge.logic.getCurrentScene().objects['pointsBig'].text = msg.bodies[0]


def pushScore(cont):
    sens = cont.sensors
    if not sens['click'].positive or not sens['over'].positive:
        return

    points = bge.logic.getCurrentScene().objects['pointsBig'].text
    bge.logic.loadGlobalDict()
    name = bge.logic.globalDict['name']

    urllib.request.urlopen("http://greybrick.kilu.de/highscore.php?" +
                           urlencode({"name": name, "points": points, "password": "angularmobile"}))

    cont.activate(cont.actuators['remove1'])
    cont.activate(cont.actuators['remove2'])
    cont.activate(cont.actuators['goHighscore'])